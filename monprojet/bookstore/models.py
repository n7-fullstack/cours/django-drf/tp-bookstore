from datetime import datetime
from django.db import models


class Author(models.Model):
    """Model for Author"""
    lastname = models.CharField(max_length=200, blank=True, default="Anonym")
    firstname = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self) -> str:
        return f"{self.firstname} {self.lastname}"


class Book(models.Model):
    """Model for Book"""

    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Date published', null=True, blank=True, default=datetime.now)
    authors = models.ManyToManyField('Author', related_name="books")

    def __str__(self) -> str:
        return f"{self.title}"


class BookEdition(models.Model):
    """Model for Book Edition"""
    isbn = models.CharField(max_length=200, null=True, blank=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="a")
    edition_company = models.CharField(max_length=200)

    def __str__(self) -> str:
        return f"{self.isbn} : {self.book.title} chez {self.edition_company}"
