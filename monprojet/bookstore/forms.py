from tkinter.tix import Form
from django import forms

from bookstore.models import Book


class ContactForm(forms.Form):
    sender = forms.EmailField(required=False)
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    cc_myself = forms.BooleanField(required=False) 

    def is_valid(self):
        if self.data["cc_myself"]:
            if self.data["sender"]:
                return super().is_valid()
            else:
                return False
        return super().is_valid()


class BookForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = '__all__'
