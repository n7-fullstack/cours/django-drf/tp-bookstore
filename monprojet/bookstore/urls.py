from django.urls import path, include
from bookstore import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'books', views.BookViewSet)

urlpatterns = [
    path('', views.IndexView.as_view(), name="bookstore-index"),
    path('contact/', views.contact_view, name="contact"),
    path('thanks/', views.ThanksView.as_view(), name="thanks"),
    path('book/list', views.BookListView.as_view(), name="book-list"),
    path('book/<int:pk>', views.BookDetailView.as_view(), name="book-detail"),
    path('book/<int:pk>/edit/', views.BookUpdateView.as_view(), name="book-update"),
    path('book/add/', views.BookCreateView.as_view(), name="book-create"),
    path('api/v0', views.api_root_view, name='api_root'),
    path('api/v0/books', views.APIBookListView.as_view(), name='api-book-list'),
    path('api/v0/books/<int:pk>', views.APIBookDetailView.as_view(), name='api-book-detail'),
    path('api/v0/authors', views.APIAuthorsListCreateView.as_view(), name='api-authors-list'),
    path('api/v1/', include(router.urls),),
    # …
]
