from django.contrib import admin
from bookstore.models import Book, BookEdition, Author



class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'pub_date')
    date_hierarchy = 'pub_date'
    search_fields = ['title']
    list_filter = ['pub_date', 'authors']


admin.site.register(Book, BookAdmin)
admin.site.register(BookEdition)
admin.site.register(Author)
