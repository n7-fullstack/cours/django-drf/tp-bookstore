import datetime

from django.urls import reverse_lazy
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.views.generic import (
    CreateView, DetailView, TemplateView,
    ListView, UpdateView
)

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework import viewsets
from rest_framework.parsers import JSONParser

from bookstore.models import Author, Book, BookEdition
from bookstore.forms import BookForm, ContactForm
from bookstore.serializers import AuthorSerializer, BookSerializer

# Create your views here.

class IndexView(ListView):
    """Vue index"""
    model = Book
    context_object_name = "latest_books"
    template_name = "bookstore/index.html"
    queryset = Book.objects.order_by('pub_date')[:2]


def contact_view(request):
    """View for contact"""
    if request.method == 'POST':
        form = ContactForm(request.POST)  # instanciation avec données
        if form.is_valid():
            # Traite les données dans form.cleaned_data puis redirige
            send_mail(
                form.cleaned_data['subject'],
                form.cleaned_data['message'],
                form.cleaned_data['sender'],
                ['to@example.com'],
                fail_silently=False,
            )
            if form.cleaned_data['cc_myself']:
                send_mail(
                    form.cleaned_data['subject'],
                    form.cleaned_data['message'],
                    form.cleaned_data['sender'],
                    form.cleaned_data['sender'],
                    fail_silently=False,
                )
            return redirect(reverse_lazy('thanks'))
    else:
        form = ContactForm()  # instanciation sans données
    # Affichage du formulaire via un template
    return render(request, 'contact.html', {'form': form})


class ThanksView(TemplateView):
    template_name = "thanks.html"


class BookListView(ListView):
    """Book list view"""
    model = Book
    #context_object_name = 'books'


class BookDetailView(DetailView):
    """Book detail view"""
    model = Book

def api_root_view(request):
    return HttpResponse("Bienvenue dans l'API")


class APIBookListView(ListCreateAPIView):
    """API V0 book list"""
    queryset = Book.objects.all()
    serializer_class = BookSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class APIBookDetailView(APIView):
    """API V0 book detail"""
    def get(self, request, pk, format=None):
        book = Book.objects.get(id=pk)
        books_serializer = BookSerializer(book)
        return Response(books_serializer.data)

    def put(self, request, pk, format=None):
        book = Book.objects.get(id=pk)
        data = request.data
        serializer = BookSerializer(book, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(data, status=201)
    
    def delete(self, request, pk, format=None):
        book = Book.objects.get(id=pk)
        book.delete()
        return Response(status=204)


class BookCreateView(CreateView):
    """Book create view"""
    model = Book
    fields = '__all__'
    template_name = 'bookstore/book_form.html'
    success_url = reverse_lazy('book-list')


class BookUpdateView(UpdateView):
    """Book create view"""
    model = Book
    fields = '__all__'
    template_name = 'bookstore/book_form.html'
    success_url = reverse_lazy('book-list')


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all().order_by("title")
    serializer_class = BookSerializer


class APIAuthorsListCreateView(ListCreateAPIView):
    """API V0 authors list"""
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
