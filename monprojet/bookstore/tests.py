from django.test import TestCase
from bookstore.models import Book


class TestBook(TestCase):
    """Tests for book"""
    def setUp(self):
        self.book1 = Book.objects.create(title="Les deux tours")
        self.book2 = Book.objects.create(title="La communauté de l'anneau")

    def test_resolve_home_url(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Bienvenue')

    def test_resolve_book_list_url(self):
        response = self.client.get('/book/list')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Les deux tours')
        self.assertEqual(response.context['object_list'].count(), 2)
        self.assertContains(response, "La communauté de l'anneau", html=True)
    
    def test_book_count(self):
        book_list = Book.objects.all()
        self.assertEqual(book_list.count(), 2)

    def test_resolve_book_detail_url(self):
        response = self.client.get(f'/book/{self.book1.id}')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, f'{self.book1.title}')
